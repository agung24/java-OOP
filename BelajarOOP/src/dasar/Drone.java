/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dasar;

/**
 *
 * @author Portgas D Ace
 */
public class Drone {
    //atribut
    int energi;
    int ketinggian;
    int kecepatan;
    String merk;
    
    void terbang(){
        energi--;
        if(energi > 10){
            ketinggian++;
            System.out.println("Drone terbang . . .");
        }
        else
        {
            System.out.println("Energi lemah: Drone gabisa terbang . . .");
        }
    }
    
    void matikanMesin(){
        if(ketinggian > 0){
            System.out.println("mesin tidak bisa dimatikan karena sedang terbang");
        }
        else
        {
            System.err.println("mesin dimatikan...");
        }
    }
    
    void turun(){
        ketinggian--;
        energi--;
        System.out.println("Drone turun");
    }
    
    void belok(){
        energi--;
        System.out.println("Drone belok");
    }
    
    void maju(){
        energi--;
        System.out.println("drone mundur");
        kecepatan++;
    }
    
    
}
