/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Turunan;

/**
 *
 * @author Portgas D Ace
 */
public class Kakek {
    protected String namekakek;
    protected String address;
    
    public Kakek()
    {
        System.out.println("\n Program Inheritance");
        System.out.println("=========================");
        System.out.println(" Masukan construktor kakek ");
        System.out.println("-Dijalankan oleh class Bapak-");
        namekakek="Doni";
        address="Merauke";
    }
    
    public Kakek(String namekakek, String address)
    {
        this.namekakek=namekakek;
        this.address=address;
    }
    
    public String getName()
    {
        return namekakek;
    }
    
    public String getAddress()
    {
        return address;
    }
}
