/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas3;

/**
 *
 * @author Portgas D Ace
 */
public class Chief extends Person{
    public Chief(int ID, String name, Double monthSalary) {
        super(ID, name, monthSalary);
    }
    
    public double AnnSalary()
    {
        return super.AnnSalary()+2000;
    }
    
    public void View()
    {
        super.getPerson();
        System.out.println("Gaji pertahun : "+AnnSalary());
        System.out.println("============================");
    }
}
