/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas3;

/**
 *
 * @author Portgas D Ace
 */
public class Main {
    public static void main(String[] args) {
        
        Director d = new Director(1, "agung", 1500.00);
        Manager m = new Manager(2, "pradhika", 2000.00);
        Chief c = new Chief(3, "nugraha", 1000.00);
        
        d.View();
        m.View();
        c.View();
    }
}
