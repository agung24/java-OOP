/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas3;

/**
 *
 * @author Portgas D Ace
 */
public class Person {
    String name;
    Double monthSalary;
    int ID;

    public Person(int ID, String name, Double monthSalary) {
        this.ID = ID;
        this.name = name;
        this.monthSalary = monthSalary;
    }
    
    public double AnnSalary()
    {
        return monthSalary*12;
    }
    
    public void getPerson()
    {
        System.out.println("ID : "+ID);
        System.out.println("Name : "+name);
        System.out.println("Gaji Perbulan : "+monthSalary);
    }
}
