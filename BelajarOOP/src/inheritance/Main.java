/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author Portgas D Ace
 */
public class Main {
    public static void main(String[] args){
        
        BangunDatar mBangunDatar = new BangunDatar();
        
        Persegi mPersegi = new Persegi();
        mPersegi.sisi = 2;
        
        Lingkaran mLingkaran = new Lingkaran();
        mLingkaran.r = 22;
        
        PersegiPanjang mPersegiPanjang = new PersegiPanjang();
        mPersegiPanjang.panjang = 8;
        mPersegiPanjang.lebar = 4;
        
        Segitiga mSegitiga = new Segitiga();
        mSegitiga.alas = 12;
        mSegitiga.tinggi = 8;
        
        mBangunDatar.luas();
        mBangunDatar.keliling();
        
        mPersegi.luas();
        mPersegi.keliling();
        
        mLingkaran.luas();
        mLingkaran.keliling();
        
        mPersegiPanjang.luas();
        mPersegiPanjang.keliling();
        
        mSegitiga.luas();
        mSegitiga.keliling();
    }
}
