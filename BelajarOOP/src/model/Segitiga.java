/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sahong D Portgas
 */
public class Segitiga {
    int alas;
    int tinggi;
    int sisiA;
    int sisiB;
    int sisiC;
 
    public int getAlas(){
        return alas;
    }
    
    public int getTinggi(){
        return tinggi;
    }
    
    public void setAlas(int alas, int tinggi){
        this.alas=alas;
        this.tinggi=tinggi;
    }
    
    public double getLuas(){
        return 0.5 * alas * tinggi;
    }
    
    public double getKeliling(){
        return sisiA + sisiB + sisiC;
    }
    
    public Segitiga(int rad){ 
        alas=rad;
        tinggi=rad;
        sisiA=rad;
        sisiB=rad;
        sisiC=rad;
    }
}
