/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sahong D Portgas
 */
public class Persegi {
    int s;
    
    public int getSisi(){
        return s;
    }
    
    public void setSisi(int s){
        this.s = s;
    }
    
    public double getLuas(){
        return s * s;
    }
    
    public double getKeliling(){
        return 4 * s;
    }
    
    public Persegi(int rad){ 
        s = rad;
    }
}
