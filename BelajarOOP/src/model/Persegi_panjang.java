/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sahong D Portgas
 */
public class Persegi_panjang {
    int panjang;
    int lebar;
    
    public int getPanjang(){
        return panjang;
    }
    
    public int getLebar(){
        return lebar;
    }
    
    public void setArea(int p, int l ){
        this.panjang = p;
        this.lebar = l;
        
    }
    
    public double getLuas(){
        return panjang * lebar;
    }
    
    public double getKeliling(){
        return 2 * (panjang + lebar);
    }
    
    public Persegi_panjang(int rad){ 
        panjang = rad;
        lebar = rad;
    }
}
