/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sahong D Portgas (Agung Pradhika Nugraha)
 */
public class Lingkaran {
    int r;
    // r adalah variable jari-jari untuk digunakan menghitung luas dan keliling
    public int getr(){
        return r;
    }
    
    public void setr(int r){
        this.r=r;
    }
    
    public double getLuas(){
        return 3.14 * r * r;
    }
    
    public double getKeliling(){
        return 3.14 * 2 * r;
    }
    
    public Lingkaran(int rad){ 
        r=rad;
    }
    
}
