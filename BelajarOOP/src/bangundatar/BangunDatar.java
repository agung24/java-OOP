/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangundatar;

import model.Lingkaran;
import model.Persegi;
import model.Segitiga;
import model.Persegi_panjang;
import java.util.Scanner;

/**
 *
 * @author Sahong D Portgas (Agung Pradhika Nugraha)
 */
public class BangunDatar {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here 
        
        // SET NILAI LINGKARAN
        Lingkaran luas = new Lingkaran(5);
        Lingkaran keliling = new Lingkaran(5);
        // SET NILAI SEGITIGA
        Segitiga Sluas = new Segitiga(8);
        Segitiga Skeliling = new Segitiga(8);
        // SET NILAI PERSEGI
        Persegi Pluas = new Persegi(12);
        Persegi Pkeliling = new Persegi(12);
        // SET NILAI PERSEGI PANJANG
        Persegi_panjang PPluas = new Persegi_panjang(4);
        Persegi_panjang PPkeliling = new Persegi_panjang(5);
        
         // PILIHAN
        Scanner input = new Scanner(System.in);
        int a;
        
        // OUTPUT PENJUMLAHAN BANGUN DATAR 
        System.out.println("--> Menghitung LUAS dan KELILING Bangun Datar <--");
        System.out.println("1.=> Lingkaran \n2.=> Segitiga\n3.=> Persegi\n4.=> Persegi panjang\n");
        System.out.println("Masukan pilihan = ");
        a = input.nextInt();
        int masukan=0;
        switch (a) {
            case 1:
                System.out.println("Luas Lingkaran : " +luas.getLuas());
                System.out.println("Keliling Lingkaran : " +keliling.getKeliling());
                break;
            case 2:
                System.out.println("Luas Segitiga : " +Sluas.getLuas());
                System.out.println("Keliling Segitiga : " +Skeliling.getKeliling());
                break;
            case 3:
                System.out.println("Luas Persegi : " +Pluas.getLuas());
                System.out.println("Keliling Persegi : " +Pkeliling.getKeliling());
                break;
            case 4:
                System.out.println("Luas Persegi Panjang : " +PPluas.getLuas());
                System.out.println("Keliling Persegi Panjang : " +PPkeliling.getKeliling());
                break;
            default:
                System.out.println("pilihan salah");
                break;
        }
        
    }
    
    
}

